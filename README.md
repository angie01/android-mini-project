# GreetingApp - Android application

## Notes & clarifications
- The application needs a user name in order to send greetings.
- If the users tries to continue any action where a text field is empty, he will received a dialog message saying that empty fields are not admitted.
- With no connection, the user will receive an error dialog telling him to try again when the phone has a working internet connection.
- The user can go back to the Welcome Screen to change his user name if needed through the option "Stop greeting" in the action bar.
- Flavors are demo and premium. The "test" version was changed to "demo" version due to gradle not admitting any flavour name that starts with "test".
- Greetings are saved at "/sdcard/GreetingApp/"(external storage) location as "<userName>_greetings.txt". This was made for easy access to the text file.
- Due to not having a specific REST Web Service, the greeting response comes from "http:/rest-service.guides.spring.io/greeting". The response is modified in the representation class so it matches only the "Hello" part instead of having a "Hello, World! <friendName>".

## Changelog

### Version 1.0
- AsyncTask implemented for saving greetings to an external file.
- Deleted unused option in toolbar.

### Version 0.8
- IntentService for http request implemented.
- Broadcast receiver implemented

### Version 0.5
- Navigation and interface working.