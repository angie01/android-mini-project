package com.example.android.greetingapp.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Created by HPEliteBook on 02/06/2016.
 */
public class SharedPrefManager {

    //Shared preferences
    SharedPreferences shPref;

    Editor editor;
    Context _context;
    //Shared prefs mode
    int PRIVATE_MODE = 0;

    //Shared prefs file name
    private static final String PREF_NAME = "GreetingApp";

    private static final String USER_NAME = "userName";

    public SharedPrefManager(Context context){
        this._context = context;
        shPref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = shPref.edit();
    }

    public void saveUserName(String userName){
        editor.putString(USER_NAME, userName);

        //Commit changes
        editor.commit();
    }

    public String loadUserName(){
        return shPref.getString(USER_NAME, "Empty");
    }
}
