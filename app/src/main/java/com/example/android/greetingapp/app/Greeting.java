package com.example.android.greetingapp.app;

/**
 * Created by HPEliteBook on 03/06/2016.
 * Representation class to receive JSON response from RESTful API
 */
public class Greeting {
    private String id;
    private String content;

    public String getId(){
        return this.id;
    }

    public String getContent(){
        String responseContent[] = this.content.split(",");

        return responseContent[0];
    }
}
