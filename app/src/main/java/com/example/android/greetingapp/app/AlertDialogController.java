package com.example.android.greetingapp.app;

import android.content.Context;

import com.example.android.greetingapp.helper.SaveFilesManager;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by HPEliteBook on 03/06/2016.
 */
public class AlertDialogController {
    //Log cat tag

    //Undefined loading time dialog
    private SweetAlertDialog loading;

    /**
     * Show a warning preset of sweet alert
     * @param context activity context
     * @param title dialog title
     * @param message dialog message
     */
    public void showWarningDialog(Context context, String title, String message){
        SweetAlertDialog warning = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        warning.setTitleText(title);
        warning.setContentText(message);
        warning.show();
    }

    /**
     * Show  error preset of sweet alert
     * @param context activity context
     * @param title dialog title
     * @param message dialog message
     */
    public void showErrorDialog(Context context, String title, String message){
        SweetAlertDialog error = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        error.setTitleText(title);
        error.setContentText(message);
        error.show();
    }

    /**
     * Show a progress dialog
     * @param context activity context
     * @param title dialog title
     */
    public void showLoadingDialog(Context context, String title){
        loading = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        loading.setTitleText(title);
        loading.show();
    }

    public void hideLoadingDialog(){
        loading.hide();
        loading.dismiss();
    }

    /**
     * Show a success dialog
     * @param context activity context
     * @param title dialog title
     * @param message dialog content text
     */
    public void showSuccessDialog(Context context, String title, String message){
        SweetAlertDialog success = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        success.setTitleText(title);
        success.setContentText(message);
        success.show();
    }

    /**
     * Shows a confirm dialog to save a file
     * @param context activity context
     * @param title dialog title
     * @param message dialog content text
     * @param dialogs activity Sweet Alert dialog
     */
    public void showConfirmationDialog(final Context context, final String title, String message,
                                       final AlertDialogController dialogs){
        SweetAlertDialog confirm = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        confirm.setTitleText(title);
        confirm.setContentText(message);
        confirm.setConfirmText("Yes");
        confirm.setCancelText("No");
        confirm.showCancelButton(true);
        //Confirm save
        confirm.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener(){
            @Override
            public void onClick(SweetAlertDialog sDialog){
                //Start AsyncTask to save file
                new SaveFilesManager(context, title, dialogs).execute();

                sDialog.dismissWithAnimation();
            }
        });
        //Cancel any action
        confirm.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener(){
            @Override
            public void onClick(SweetAlertDialog sDialog){
                //Do nothing...
                sDialog.cancel();
            }
        });

        confirm.show();
    }
}
