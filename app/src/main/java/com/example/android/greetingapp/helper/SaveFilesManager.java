package com.example.android.greetingapp.helper;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.example.android.greetingapp.activity.MainActivity;
import com.example.android.greetingapp.app.AlertDialogController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Created by HPEliteBook on 03/06/2016.
 */
public class SaveFilesManager extends AsyncTask<String, Void, Boolean> {
    //Log cat tag
    private static final String TAG = MainActivity.class.getSimpleName();

    private Context context;
    private static final String GREETING_STORAGE_FILE = "";
    private String userName;
    private String greetingContent;
    private AlertDialogController dialogs;

    public SaveFilesManager(Context _context, String _greetingContent, AlertDialogController _dialogs){
        context = _context;
        greetingContent = _greetingContent;
        dialogs = _dialogs;
        //Get user name
        SharedPrefManager prefs = new SharedPrefManager(_context);
        userName = prefs.loadUserName();
    }

    @Override
    protected void onPreExecute(){
        dialogs.showLoadingDialog(context, "Saving greeting...");
    }

    @Override
    protected Boolean doInBackground(String... params){
        //Save file here
        if(isExternalStorageWritable()){
            String fileName = userName + "_greetings.txt";
            String fileContent;
            File textFile = new File(Environment.getExternalStorageDirectory() + "/GreetingApp/" + fileName);

            if(textFile.exists()){
                //If file exist, get old contents + new content
                fileContent = getFileContent(textFile);
                fileContent += greetingContent;
            }
            else{
                //If not, get new content only and make dir if not exist
                fileContent = greetingContent;
                textFile.getParentFile().mkdir();
            }

            //Save the content to the file
            try{
                FileOutputStream fOut = new FileOutputStream(textFile);
                OutputStreamWriter outWriter = new OutputStreamWriter(fOut);
                outWriter.append(fileContent);
                outWriter.close();
            } catch(Exception e){
                //Something happened and file was not saved.
                Log.e(TAG, "Couldn't write to external storage. Error: " + e.getMessage());
                return false;
            }
        }
        else{
            return false;
        }

        return true;
    }
    @Override
    protected void onPostExecute(Boolean success){

        dialogs.hideLoadingDialog();
        if(success){
            dialogs.showSuccessDialog(context, "File Saved",
                    "File was successful saved to external storage.");
        }
        else{
            dialogs.showErrorDialog(context, "File not Saved",
                    "An error happened and your greeting couldn't be saved.");
        }
    }

    /**
     * Checks if the external storage is available and allows writing
     * @return boolean
     */
    public boolean isExternalStorageWritable(){
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)){
            return true;
        }

        return false;
    }

    /**
     * Gets the content of the previous saved file
     * @param previousFile
     * @return
     */
    private String getFileContent(File previousFile){
        try{
            FileInputStream fIn = new FileInputStream(previousFile);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fIn));
            String dataRow = "";
            String dataBuffer = "";

            while((dataRow = reader.readLine()) != null){
                dataBuffer += dataRow + "\n";
            }

            return dataBuffer;
        } catch(Exception e){
            Log.d(TAG, "File does not exist. Error: " + e.getMessage());
        }

        return null;
    }
}
