package com.example.android.greetingapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.android.greetingapp.R;
import com.example.android.greetingapp.app.AlertDialogController;
import com.example.android.greetingapp.helper.SharedPrefManager;

/**
 * Created by HPEliteBook on 31/05/2016.
 */
public class WelcomeActivity extends AppCompatActivity {

    private Button doneButton;
    private EditText userNameEdit;
    private SharedPrefManager prefs;
    private AlertDialogController dialogs;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        //Initiate UI Interactive elements
        doneButton = (Button) findViewById(R.id.startMain);
        userNameEdit = (EditText) findViewById(R.id.userName);
        //Shared preferences
        prefs = new SharedPrefManager(getApplicationContext());
        //Dialog controller
        dialogs = new AlertDialogController();

        //Done button Click event
        doneButton.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view){
                String userName = userNameEdit.getText().toString();

                if(!userName.isEmpty()){
                    startGreeting(userName);
                }
                else{
                    //No empty user name allowed
                    dialogs.showWarningDialog(WelcomeActivity.this,
                            "User name required!",
                            "Please type a name to continue.");
                }
            }
        });
    }

    /**
     * Starts MainActivity
     * @param userName Name to be saved as the sender
     */
    private void startGreeting(final String userName){
        //Save user name
        prefs.saveUserName(userName);
        //Now start the new activity
        Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
