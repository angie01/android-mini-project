package com.example.android.greetingapp.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.android.greetingapp.R;
import com.example.android.greetingapp.activity.WelcomeActivity;
import com.example.android.greetingapp.app.AlertDialogController;
import com.example.android.greetingapp.helper.ServiceHandler;
import com.example.android.greetingapp.helper.SharedPrefManager;

public class MainActivity extends AppCompatActivity {
    //Logcat tag
    private static final String TAG = MainActivity.class.getSimpleName();

    private Button sendGreeting;
    private EditText friendNameEdit;
    private SharedPrefManager prefs;
    private AlertDialogController dialogs;
    //Action bar
    private Toolbar actionBar;
    private static final String BAR_TITLE = "Welcome ";
    //Broadcast receiver
    private ResponseReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initiate the interactive UI elements
        sendGreeting = (Button) findViewById(R.id.greet_button);
        friendNameEdit = (EditText) findViewById(R.id.friendName);

        //Shared preferences
        prefs = new SharedPrefManager(getApplicationContext());
        //Dialog controller
        dialogs = new AlertDialogController();

        //Set action bar
        actionBar = (Toolbar) findViewById(R.id.actionBar);
        setSupportActionBar(actionBar);
        getSupportActionBar().setTitle(BAR_TITLE + prefs.loadUserName());

        //Send Greeting Click Event
        sendGreeting.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view){
                String friendName = friendNameEdit.getText().toString();

                if(!friendName.isEmpty()){
                    if(isNetworkAvailable()){
                        //Send greet
                        Log.d(TAG, "Your friend's name is: " + friendName);

                        dialogs.showLoadingDialog(MainActivity.this, "Sending");
                        sendToService(friendName);
                    }
                    else{
                        //Send message "No connection, try again later."
                        Log.d(TAG, "No connection found");

                        dialogs.showErrorDialog(MainActivity.this,
                                "Connection error",
                                "No connection, try again later.");
                    }
                }
                else{
                    //Send message "Empty field not admitted!"
                    Log.d(TAG, "Empty field found!");

                    dialogs.showWarningDialog(MainActivity.this,
                            "Ooppss!",
                            "Empty field not admitted!");
                }
            }
        });

        //Register broadcast receiver
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new ResponseReceiver();
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onResume(){
        super.onResume();

        //Register broadcast receiver
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause(){
        super.onPause();

        //Free the receiver
        unregisterReceiver(receiver);
    }

    /*-------Menu methods-------*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.go_back:
                returnToWelcomeScreen();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    /*-------End Menu Methods-------*/

    /**
     * Pass receiver name to IntentService
     * @param friendName friend/receiver name
     */
    private void sendToService(final String friendName){
        Intent inputIntent = new Intent(MainActivity.this, ServiceHandler.class);
        inputIntent.putExtra(ServiceHandler.RECEIVER, friendName);
        startService(inputIntent);
    }

    /**
     * Go back to the welcome screen
     */
    private void returnToWelcomeScreen(){
        Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
        startActivity(intent);

        finish();
    }

    /**
     * It tells if the phone has internet connection at the moment
     * @return boolean
     */
    private boolean isNetworkAvailable(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null);
    }

    //Broadcast receiver
    public class ResponseReceiver extends BroadcastReceiver{
        public static final String ACTION = "com.example.android.intent_service.ALL_DONE";

        @Override
        public void onReceive(Context context, Intent intent){
            Log.d(TAG, "Broadcast receive on work");

            dialogs.hideLoadingDialog();

            boolean responseFailed = intent.getBooleanExtra(ServiceHandler.SUCCESS, false);

            if(!responseFailed){
                String friendName = intent.getStringExtra(ServiceHandler.RECEIVER);
                String greeting = intent.getStringExtra(ServiceHandler.GREETING);

                dialogs.showConfirmationDialog(MainActivity.this,
                        greeting +  " " + friendName, "Do you want to save this greeting?", dialogs);
            }
            else{
                Log.d(TAG, "Request failed");

                dialogs.showErrorDialog(MainActivity.this,
                        "Service not available",
                        "The server couldn't process your greeting. Try again later");
            }
        }
    }
}
