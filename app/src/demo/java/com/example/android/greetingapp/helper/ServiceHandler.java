package com.example.android.greetingapp.helper;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.example.android.greetingapp.activity.MainActivity;
import com.example.android.greetingapp.app.Greeting;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by HPEliteBook on 31/05/2016.
 */
public class ServiceHandler extends IntentService {
    public static final String TAG = MainActivity.class.getSimpleName();

    public static final String RECEIVER = "friendName";
    public static final String GREETING = "greeting";
    public static final String SUCCESS = "requestSuccess";
    //Constructor
    public ServiceHandler(){
        super(ServiceHandler.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent workIntent){
        //Get data from the incoming intent
        String receiver = workIntent.getStringExtra(RECEIVER);
        Greeting greeting = null;
        boolean requestFailed;
        //try connection
        try{
            final String url = "http://rest-service.guides.spring.io/greeting";
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            greeting = restTemplate.getForObject(url, Greeting.class);
            requestFailed = false;

            Log.d(TAG, "http request attempted");

        } catch(Exception e){
            //Http request failed
            Log.e(TAG, e.getMessage(), e);

            requestFailed = true;
        }

        //Broadcast Spring response
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(MainActivity.ResponseReceiver.ACTION);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);

        if(!requestFailed){
            broadcastIntent.putExtra(RECEIVER, receiver);
            broadcastIntent.putExtra(GREETING, greeting.getContent());
        }
        broadcastIntent.putExtra(SUCCESS, requestFailed);

        Log.d(TAG, "Broadcast made.");

        sendBroadcast(broadcastIntent);
    }

}